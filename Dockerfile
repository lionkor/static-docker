ARG ALPINE_TAG=edge
FROM alpine:${ALPINE_TAG}

RUN apk update && apk --no-cache add binutils cmake make libgcc musl-dev clang gcc g++

ENV CXX=clang++ CC=clang LDFLAGS=-static

CMD [ "/bin/sh" ]
